import { NestFactory } from '@nestjs/core';
import { async } from 'rxjs';
import { AppModule } from './app.module';

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule);
  // app.setGlobalPrefix('api');
  await app.listen(3000);
};

bootstrap();
