import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScoresModule } from './modules/scores/scores.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
    imports: [
        ScoresModule,
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '..', '/src/public/views/game'),
            exclude: ['/api*'],
            renderPath: '/game'
        }),
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
